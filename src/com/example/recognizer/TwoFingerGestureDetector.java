package com.example.recognizer;

import android.view.MotionEvent;

public class TwoFingerGestureDetector {

	public interface OnTwoFingerListener {
		boolean onTwoFingerTap(int x1, int y1, int x2, int y2);
	}
	
	private static final int STATE_INIT = 1;
	private static final int STATE_DOWN = 2;
	private static final int STATE_PTR_DOWN = 3;
	private static final int STATE_PTR_UP = 4;
	private static final int STATE_FINI = 5;
	
	private static final float MAX_PIXEL_DELTA = 10.0f;
	private static final long MAX_TIME_DELTA = 150;
	
	private OnTwoFingerListener mListener;
	
	private int mState;
	private long mDowntime;
	
	private int mId1;
	private float mX1;
	private float mY1;
	
	private int mId2;
	private float mX2;
	private float mY2;
	
	public TwoFingerGestureDetector(OnTwoFingerListener listener) {
		mListener = listener;
		mState = STATE_INIT;
	}
	
	public boolean onTouchEvent(MotionEvent me){
		boolean handled = process(me);
		
		if (getState() == STATE_FINI) {
			mListener.onTwoFingerTap((int)mX1, (int)mY1, (int)mX2, (int)mY2);
			reset();
		}
		
		return handled;
	}
	
	private void reset() {
		setState(STATE_INIT);
		mDowntime = mId1 = mId2 = -1000;
		mX1 = mY1 = mX2 = mY2 = -1000.0f;
	}
	
	private boolean process(MotionEvent me) {
		
		boolean handled = false;
		//int action = me.getActionMasked();
		//int index = me.getActionIndex();
		int action = me.getAction() & MotionEvent.ACTION_MASK;
		int index = (me.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;		
		int count = me.getPointerCount();

		// do some pre-processing
		if (action != MotionEvent.ACTION_DOWN) {
			if (mDowntime != me.getDownTime()
					|| !withinDowntimeDelta(me.getEventTime())
					|| count > 2) {
				reset();
				return handled;
			}
		}
		
		int curState = mState;
		switch (curState) {
		case STATE_INIT:
			if (action == MotionEvent.ACTION_DOWN) {
				mDowntime = me.getDownTime();
				mId1 = me.getPointerId(index);
				mX1 = me.getX(index);
				mY1 = me.getY(index);
				setState(STATE_DOWN);
				handled = true;
			}
			break;
		case STATE_DOWN:
			switch (action) {
			case MotionEvent.ACTION_POINTER_DOWN:
				mId2 = me.getPointerId(index);
				mX2 = me.getX(index);
				mY2 = me.getY(index);
				setState(STATE_PTR_DOWN);
				handled = true;
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_CANCEL:
				reset();
				break;
			}
			break;
		case STATE_PTR_DOWN:
			switch (action) {
			case MotionEvent.ACTION_POINTER_UP:
				if (!withinPixelDelta(me.getPointerId(index), me.getX(index), me.getY(index))) {
					reset();
				} else {
					setState(STATE_PTR_UP);
					handled = true;
				}
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:				
			case MotionEvent.ACTION_CANCEL:
				reset();
				break;
			}
			break;
		case STATE_PTR_UP:
			switch (action) {
			case MotionEvent.ACTION_UP:
				if (!withinPixelDelta(me.getPointerId(index), me.getX(index), me.getY(index))) {
					reset();
				} else {
					setState(STATE_FINI);
					handled = true;
				}
				break;
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:				
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_POINTER_UP:
				reset();
				break;
			}
			break;
		case STATE_FINI:
			// should not be here
			break;
		}
		return handled;
	}
	
	private boolean withinPixelDelta(int id, float x, float y) {

		float mx = 1000;
		float my = 1000;
		
		if (id == mId1) {
			mx = mX1;
			my = mY1;
		} else if (id == mId2) {
			mx = mX2;
			my = mY2;
		}
		
		return Math.abs(mx - x) <= MAX_PIXEL_DELTA && Math.abs(my - y) <= MAX_PIXEL_DELTA;
	}
	
	private boolean withinDowntimeDelta(long t) {
		return Math.abs(t - mDowntime) <= MAX_TIME_DELTA;
	}
	
	
	private int getState() {
		return mState;
	}
	
	private void setState(int state) {
		mState = state;
	}
	
	
}
