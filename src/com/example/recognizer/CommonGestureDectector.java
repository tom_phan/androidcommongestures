package com.example.recognizer;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.SimpleOnGestureListener;

public class CommonGestureDectector {

    /**
     * The listener that is used to notify when gestures occur.
     * If you want to listen for all the different gestures then implement
     * this interface. If you only want to listen for a subset it might
     * be easier to extend {@link SimpleOnGestureListener}.
     */
    public interface OnCommonGestureListener {

        /**
         * Notified when a tap occurs with the down {@link MotionEvent}
         * that triggered it. This will be triggered immediately for
         * every down event. All other events should be preceded by this.
         *
         * @param e The down motion event.
         */
        boolean onDown(MotionEvent e);

        /**
         * The user has performed a down {@link MotionEvent} and not performed
         * a move or up yet. This event is commonly used to provide visual
         * feedback to the user to let them know that their action has been
         * recognized i.e. highlight an element.
         *
         * @param e The down motion event
         */
        void onShowPress(MotionEvent e);

        /**
         * Notified when a tap occurs with the up {@link MotionEvent}
         * that triggered it.
         *
         * @param e The up motion event that completed the first tap
         * @return true if the event is consumed, else false
         */
        boolean onSingleTapUp(MotionEvent e);

        /**
         * Notified when a scroll occurs with the initial on down {@link MotionEvent} and the
         * current move {@link MotionEvent}. The distance in x and y is also supplied for
         * convenience.
         *
         * @param e1 The first down motion event that started the scrolling.
         * @param e2 The move motion event that triggered the current onScroll.
         * @param distanceX The distance along the X axis that has been scrolled since the last
         *              call to onScroll. This is NOT the distance between {@code e1}
         *              and {@code e2}.
         * @param distanceY The distance along the Y axis that has been scrolled since the last
         *              call to onScroll. This is NOT the distance between {@code e1}
         *              and {@code e2}.
         * @return true if the event is consumed, else false
         */
        boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);

        /**
         * Notified when a long press occurs with the initial on down {@link MotionEvent}
         * that trigged it.
         *
         * @param e The initial on down motion event that started the longpress.
         */
        void onLongPress(MotionEvent e);

        /**
         * Notified of a fling event when it occurs with the initial on down {@link MotionEvent}
         * and the matching up {@link MotionEvent}. The calculated velocity is supplied along
         * the x and y axis in pixels per second.
         *
         * @param e1 The first down motion event that started the fling.
         * @param e2 The move motion event that triggered the current onFling.
         * @param velocityX The velocity of this fling measured in pixels per second
         *              along the x axis.
         * @param velocityY The velocity of this fling measured in pixels per second
         *              along the y axis.
         * @return true if the event is consumed, else false
         */
        boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);

        /**
         * Notified when a single-tap occurs.
         * <p>
         * Unlike {@link OnGestureListener#onSingleTapUp(MotionEvent)}, this
         * will only be called after the detector is confident that the user's
         * first tap is not followed by a second tap leading to a double-tap
         * gesture.
         *
         * @param e The down motion event of the single-tap.
         * @return true if the event is consumed, else false
         */
        boolean onSingleTapConfirmed(MotionEvent e);
 
        /**
         * Notified when a double-tap occurs.
         *
         * @param e The down motion event of the first tap of the double-tap.
         * @return true if the event is consumed, else false
         */
        boolean onDoubleTap(MotionEvent e);

        /**
         * Notified when an event within a double-tap gesture occurs, including
         * the down, move, and up events.
         *
         * @param e The motion event that occurred during the double-tap gesture.
         * @return true if the event is consumed, else false
         */
        boolean onDoubleTapEvent(MotionEvent e);
        
        /**
         * Notified when a two finger-tap occurs.
         *
         * @param x1 The x of down motion event.
         * @param y1 The y of down motion event.
         * @param x2 The x of up motion event.
         * @param y2 The y of up motion event.
         * @return true if the event is consumed, else false
         */
        boolean onTwoFingerTap(int x1, int y1, int x2, int y2);
        
        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *          retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         *          as handled. If an event was not handled, the detector
         *          will continue to accumulate movement until an event is
         *          handled. This can be useful if an application, for example,
         *          only wants to update scaling factors if the change is
         *          greater than 0.01.
         */
        public boolean onScale(ScaleGestureDetector detector);

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *          retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         *          this gesture. For example, if a gesture is beginning
         *          with a focal point outside of a region where it makes
         *          sense, onScaleBegin() may return false to ignore the
         *          rest of the gesture.
         */
        public boolean onScaleBegin(ScaleGestureDetector detector);

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         *
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *          retrieve extended info about event state.
         */
        public void onScaleEnd(ScaleGestureDetector detector);        
    }	
    
    public static class SimpleOnCommonGestureListener implements OnCommonGestureListener {

		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			return false;
		}

		@Override
		public void onLongPress(MotionEvent e) {
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			return false;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onDoubleTapEvent(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onTwoFingerTap(int x1, int y1, int x2, int y2) {
			return false;
		}

		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			return false;
		}

		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) {
			return false;
		}

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) {
		}
    }
	
    GestureDetector.SimpleOnGestureListener mSimpleOnGestureListener = new GestureDetector.SimpleOnGestureListener() {
    	
    	@Override
        public boolean onSingleTapUp(MotionEvent e) {
        	Logger.log("onSingleTapUp");
            return mCommonGestureListener.onSingleTapUp(e);
        }

    	@Override
        public void onLongPress(MotionEvent e) {
        	Logger.log("onLongPress");
        	mCommonGestureListener.onLongPress(e);
        }

    	@Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                float distanceX, float distanceY) {
        	Logger.log("onScroll");
            return mCommonGestureListener.onScroll(e1, e2, distanceX, distanceY);
        }

    	@Override    	
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                float velocityY) {
        	Logger.log("onFling");
            return mCommonGestureListener.onFling(e1, e2, velocityX, velocityY);
        }
    	
    	@Override
        public void onShowPress(MotionEvent e) {
        	Logger.log("onShowPress");
        	mCommonGestureListener.onShowPress(e);
        }

    	@Override
        public boolean onDown(MotionEvent e) {
        	Logger.log("onDown");
            return mCommonGestureListener.onDown(e);
        }

    	@Override
        public boolean onDoubleTap(MotionEvent e) {
        	Logger.log("onDoubleTap");
            return mCommonGestureListener.onDoubleTap(e);
        }

    	@Override
        public boolean onDoubleTapEvent(MotionEvent e) {
        	Logger.log("onDoubleTapEvent");
            return mCommonGestureListener.onDoubleTapEvent(e);
        }

    	@Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
        	Logger.log("onSingleTapConfirmed");
            return mCommonGestureListener.onSingleTapConfirmed(e);
        }    	
    };
    
    ScaleGestureDetector.SimpleOnScaleGestureListener mSimpleOnScaleGestureListener = new ScaleGestureDetector.SimpleOnScaleGestureListener() {

    	@Override
        public boolean onScale(ScaleGestureDetector detector) {
        	Logger.log("onScale");
            return mCommonGestureListener.onScale(detector);
        }

    	@Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
        	Logger.log("onScaleBegin");
            return mCommonGestureListener.onScaleBegin(detector);
        }

    	@Override
        public void onScaleEnd(ScaleGestureDetector detector) {
        	Logger.log("onScaleEnd");
        	mCommonGestureListener.onScaleEnd(detector);
        }    	
    };
    
    TwoFingerGestureDetector.OnTwoFingerListener mOnTwoFingerGestureListener = new TwoFingerGestureDetector.OnTwoFingerListener() {

		@Override
		public boolean onTwoFingerTap(int x1, int y1, int x2, int y2) {
			Logger.log("onTwoFingerTap");
			return mCommonGestureListener.onTwoFingerTap(x1, y1, x2, y2);
		}
    	
    };
    
	static class Logger {
		static boolean DEBUG = true;
		static boolean DEBUG_ME = true;
		static String TAG = "CommonGestureDetector";

		static void log(String msg) {
			if (DEBUG) {
				Log.d(TAG, msg);
			}
		}

		static void log_ts(MotionEvent me) {
			Log.d(TAG, me.toString());
		}

		static void log_impl(MotionEvent me) {
			int action = me.getAction();
			
			int index = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			int aindex = me.getActionIndex();
			assert(index == aindex);
			
			StringBuilder msg = new StringBuilder();
			msg.append("MotionEvent { action=").append(
					actionToString(me.getAction()));

			final int pointerCount = me.getPointerCount();
			for (int i = 0; i < pointerCount; i++) {
				msg.append(", id[").append(i).append("]=")
						.append(me.getPointerId(i));
				msg.append(", x[").append(i).append("]=").append(me.getX(i));
				msg.append(", y[").append(i).append("]=").append(me.getY(i));
			}

			msg.append(", pointerCount=").append(pointerCount);
			msg.append(", historySize=").append(me.getHistorySize());
			msg.append(", eventTime=").append(me.getEventTime());
			msg.append(", downTime=").append(me.getDownTime());
			msg.append(", deviceId=").append(me.getDeviceId());
			msg.append(" }");
			Log.d(TAG, msg.toString());
		}

		static void log(MotionEvent me) {
			if (DEBUG_ME) {
				//log_ts(me);
				log_impl(me);
			}
		}
	}

	public static String actionToString(int action) {
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			return "ACTION_DOWN";
		case MotionEvent.ACTION_UP:
			return "ACTION_UP";
		case MotionEvent.ACTION_CANCEL:
			return "ACTION_CANCEL";
		case MotionEvent.ACTION_OUTSIDE:
			return "ACTION_OUTSIDE";
		case MotionEvent.ACTION_MOVE:
			return "ACTION_MOVE";
		}
		int index = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;

		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_POINTER_DOWN:
			return "ACTION_POINTER_DOWN(" + index + ")";
		case MotionEvent.ACTION_POINTER_UP:
			return "ACTION_POINTER_UP(" + index + ")";
		default:
			return Integer.toString(action);
		}
	}
    
    Context mContext;
	GestureDetector mSimpleGestureDetector;
	ScaleGestureDetector mScaleGestureDetector;
	TwoFingerGestureDetector mTwoFingerGestureDetector;
	OnCommonGestureListener mCommonGestureListener;
	
	

	public CommonGestureDectector(Context context, OnCommonGestureListener commonGestureListener) {
		mContext = context; //context.getApplicationContext();
		mCommonGestureListener = commonGestureListener;
		mScaleGestureDetector = new ScaleGestureDetector(mContext, mSimpleOnScaleGestureListener);
		mSimpleGestureDetector = new GestureDetector(mContext, mSimpleOnGestureListener);
		mTwoFingerGestureDetector = new TwoFingerGestureDetector(mOnTwoFingerGestureListener);
	}
	
    public boolean onTouchEvent(MotionEvent event) {
    	Logger.log(event);
        boolean retVal = false;
        retVal = mTwoFingerGestureDetector.onTouchEvent(event);
        retVal = mScaleGestureDetector.onTouchEvent(event) || retVal;
        retVal = mSimpleGestureDetector.onTouchEvent(event) || retVal;
        return retVal;
    }
	
}
