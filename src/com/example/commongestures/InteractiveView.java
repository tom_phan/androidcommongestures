package com.example.commongestures;

import com.example.recognizer.CommonGestureDectector;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

public class InteractiveView extends View {


	CommonGestureDectector mCommonGestureDectector;

	public InteractiveView(Context context) {
		super(context);
		init(context);
	}
	
	public InteractiveView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	 public InteractiveView(Context context, AttributeSet attrs, int defStyle) {
		 super(context, attrs, defStyle);
		 init(context);
	 }
	 
	 private void init(Context context) {
		 mCommonGestureDectector = new CommonGestureDectector(context, 
				 new CommonGestureDectector.SimpleOnCommonGestureListener() {
			 @Override
			 public boolean onScaleBegin(ScaleGestureDetector detector) {
				 return true;
			 }
		 });
	 }
	 
		
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
		 boolean handled = mCommonGestureDectector.onTouchEvent(event);
		 return handled || super.onTouchEvent(event);
	 }	 
}
